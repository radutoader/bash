echo "load .bash_profile"
source ~/.profile

#mac ports
export PATH="/opt/local/bin:/opt/local/sbin:/usr/bin:/usr/sbin/:/usr/local/bin:/usr/local/sbin:$PATH"

source ~/.bashrc


eval "$(/opt/homebrew/bin/brew shellenv)"

source "$(brew --prefix)/etc/bash_completion"


# bash-completion mac ports
if [ -f /opt/local/etc/profile.d/bash_completion.sh ]; then
  . /opt/local/etc/profile.d/bash_completion.sh
fi

source /opt/local/share/bash-completion/completions/git

alias k=kubectl

####  # First, add the new shell to the list of allowed shells.
#### sudo bash -c 'echo /usr/local/bin/bash >> /etc/shells'
####  # Change to the new shell.
#### chsh -s /usr/local/bin/bash
#### echo $BASH_VERSION


source <(kubectl completion bash)
complete -o default -F __start_kubectl k
#source ~/.git-completion.bash

export PATH="$PATH:"/Applications/microchip/xc16/v1.26/bin""
export PATH="$PATH:"/Applications/microchip/xc32/v1.40/bin""
export PATH="$PATH:"/Applications/microchip/xc8/v1.36/bin""


export CURL_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt
export REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/Users/radu.toader/.sdkman"
[[ -s "/Users/radu.toader/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/radu.toader/.sdkman/bin/sdkman-init.sh"


# Setting PATH for Python 3.11
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.11/bin:${PATH}"
export PATH


PATH="/Applications/IntelliJ IDEA.app/Contents/MacOS:${PATH}"
export PATH

alias pip=pip3
alias python=python3

# Added by Toolbox App
export PATH="$PATH:/usr/local/bin"
