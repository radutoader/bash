
#echo "load .bashrc " // dont echo in this file, scp won't work anymore
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

#http://stackoverflow.com/questions/9457233/unlimited-bash-history
HISTSIZE=5000
HISTFILESIZE=5000

if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

alias grep="grep --color"

export EDITOR=vim
export LESS=eFRX


alias ij="open -a /Applications/IntelliJ\ IDEA.app/"
alias code="open -a /Applications/Visual\ Studio\ Code.app/"


export GOPATH=$HOME/go
export GOROOT=/usr/local/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

function gi() { curl -L -s https://www.gitignore.io/api/$@ ;}

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
