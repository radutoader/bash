echo "load .profile"

export PATH=$PATH:/usr/local/mysql/bin

#export PATH=$PATH:/opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin

#PROMPT_COMMAND='history -a;echo -en "\033[m\033[38;5;2m"$(memory.py)"\t\033[m\033[38;5;55m$(uptime |sed "s/.*:\(.*\)/\1/g")\033[m $(get_git_changes)"'
PROMPT_COMMAND='history -a;echo -en "\033[38;5;46mload: $(uptime |sed "s/.*:\(.*\)/\1/g")\033[m $(get_git_changes)"'
#PROMPT_COMMAND='history -a;echo -en "\033[m$(get_git_changes)"'
export PROMPT_COMMAND

# Use this other PS1 string if you want \W for root and \w for all other users:
PS1="$(if [[ ${EUID} == 0 ]]; then echo -ne '\[\033[01;31m\]\n\h\[\033[01;34m\] \w'; else echo -ne '\[\033[01;32m\]\n\u@\h\[\033[01;34m\] \w'; fi) \$([[ \$? -ne 0 ]] && echo -ne \"\[\033[01;31m\]:(\[\033[01;34m\] \")\[\033[1;31m\]\$(parse_git_branch)\[\033[01;34m\]\\$ \[\033[00m\]"
#PS1="$(if [[ ${EUID} == 0 ]]; then echo -ne '\[\033[01;31m\]\h\[\033[01;34m\] \w'; else echo -ne '\[\033[01;32m\]\u@\h\[\033[01;34m\] \w'; fi) \$([[ \$? -ne 0 ]] && echo -ne \"\[\033[01;31m\]:(\[\033[01;34m\] \")\[\033[1;31m\]\$(parse_git_branch)\[\033[01;34m\]\\$ \[\033[00m\]"

export PS1

export LESS="-X"
export CLICOLOR=1
#white background
#export LSCOLORS=ExFxCxDxBxegedabagacad
#you can use this if you are using a black background:
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx

export PATH="$PATH:$HOME/.npm-packages/bin/"

export BASH_SILENCE_DEPRECATION_WARNING=1

source "$HOME/.cargo/env"

#alias python=python3
#alias pip=pip3
#
#
export PATH="$PATH:/opt/homebrew/bin"



#alias zspotify="python zspotify"
#alias zs='zspotify --download-real-time true --download-format mp3 --output "{playlist}/{playlist_num} - {artist} - {song_name}.{ext}"'
