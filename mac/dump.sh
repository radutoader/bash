port -qv installed | grep active | tee port.qv.installed
port installed | grep active | tee port.installed
brew list | tee brew.list
brew --cask list | tee brew.cask.list
pip freeze | tee pip.freeze
pip3 freeze | tee pip3.freeze
gem list | tee gem.list
cp ~/.vimrc .
cp ~/.bash_profile .
cp ~/.bashrc .
cp ~/.profile .
cp ~/.gitconfig .

